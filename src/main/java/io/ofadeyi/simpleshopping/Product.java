package io.ofadeyi.simpleshopping;

/**
 * Created by ofadeyi on 03/05/16.
 */
public class Product {
    private final String name;
    private final Long quantity;
    private final double price;
    private final double discountAmount;
    private final int discountTrigger;

    public Product(String name, double price, Long quantity, double discountAmount, int discountTrigger) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.discountAmount = discountAmount;
        this.discountTrigger = discountTrigger;
    }

    public String getName() {
        return name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public int getDiscountTrigger() {
        return discountTrigger;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
