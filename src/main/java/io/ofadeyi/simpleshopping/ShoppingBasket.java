package io.ofadeyi.simpleshopping;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.util.stream.Collectors.summingDouble;

/**
 * Created by ofadeyi on 03/05/16.
 */
public class ShoppingBasket {
    private static final String APPLE = "Apple";

    public double calculateTotal(List<String> shoppingList) {
        if ((shoppingList == null) || (shoppingList.isEmpty() == true)) {
            return 0.0;
        }

        Double total = shoppingList.stream().distinct()
                .map(item -> mapToProduct(shoppingList, item))
                .map(product -> applyProductDiscountRule(product))
                .collect(summingDouble(price -> price.doubleValue()));

        return total;
    }

    private Product mapToProduct(final List<String> items, final String item) {
        long quantity = items.stream()
                .filter(name -> name.equals(item))
                .count();
        double price = 0.25;
        double discountAmount = 0.66666666666667;
        int discountTrigger = 3;

        if (item.equals(APPLE)) {
            price = 0.6;
            discountAmount = 0.5;
            discountTrigger = 2;
        }

        return new Product(item, price, quantity, discountAmount, discountTrigger);
    }

    private BigDecimal applyProductDiscountRule(Product product) {
        long extraItems = product.getQuantity() % product.getDiscountTrigger();
        long discountableItems = product.getQuantity() - extraItems;

        BigDecimal discountableItemsTotal = BigDecimal.valueOf(discountableItems)
                .multiply(BigDecimal.valueOf(product.getPrice()))
                .multiply(BigDecimal.valueOf(product.getDiscountAmount()));
        BigDecimal extraItemsTotal = BigDecimal.valueOf(extraItems).multiply(BigDecimal.valueOf(product.getPrice()));

        return discountableItemsTotal.add(extraItemsTotal).setScale(2, RoundingMode.HALF_UP);
    }
}
