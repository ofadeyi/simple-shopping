package io.ofadeyi.simpleshopping

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by ofadeyi on 03/05/16.
 */
class ShoppingBasketSpec extends Specification {
    ShoppingBasket shoppingBasket

    def setup() {
        shoppingBasket = new ShoppingBasket()
    }


    def 'should have a 0.0 total value for a new shopping basket'() {

        when:
        double total = shoppingBasket.calculateTotal(Arrays.asList())

        then:
        total == 0.0
    }

    def 'should have a total greater than 0.0 total for a non empty basket'() {

        when:
        double total = shoppingBasket.calculateTotal(Arrays.asList('Apple', 'Orange'))

        then:
        total > 0.0
    }

    @Unroll
    def "should calculate the total for the basket"(def items, def total) {
        expect:
        total == shoppingBasket.calculateTotal(items)

        where:
        items                                            | total
        ['Apple', 'Apple', 'Orange', 'Apple', 'Apple']   | 1.45
        ['Orange', 'Orange', 'Orange', 'Apple', 'Apple'] | 1.10
        ['Apple', 'Apple', 'Apple', 'Apple', 'Apple', 'Orange'] | 2.05

    }


}
