Simple Shopping
-------------------------------

This a simple gradle project with the following structure:
```
  simple-shopping/
    |--+ src/
         |--+ main/
         |--+ test/
    |--+ build.gradle
    |--+ .gitignore
    |--+ gradle/
    |--+ gradlew
    |--+ gradlew.bat
```
Please run`./gradlew test` to execute all the unit tests
